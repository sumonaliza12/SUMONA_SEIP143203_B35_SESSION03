<?php

echo "use of floatval function<br>";

$var1="12.34bitm";

echo floatval($var1);

$var2="bitm12.34";

echo floatval($var2);

$var3="b123.45kl";
 echo floatval($var3);

echo "<br>use of empty function<br>";

$var4="10";

echo empty($var4);

$var5=0;
echo empty($var5);

echo "<br> use of is_array function<br>";

$yes=array('this','is','array');

echo is_array($yes) ? 'Array' : 'not an array';

echo "<br>";

$no='this is a string';

echo is_array($no) ? 'Array' : 'Not an array';


echo "<br>use of is_null function<br>";
$var6=NULL;
$var7=null;
$var8=0;
echo is_null($var6);
echo is_null($var7);
echo is_null(var8);
echo "<br>use of serialize function<br>";

$arr=serialize(array("bitm","basis"));
echo $arr;
echo "<br>use of var_export function<br>";
$b=3.1;
$v=var_export($b);
echo $v;
echo "<br>";
$a=array(1,2,array("a","b","c"));
var_export($a);

$str="I am a BITM student";
var_export($str);

$c=3;
$v=var_export($c);

echo "<br>use of unset function<br>";
 function uns()
 {
     static $var;
     $var++;
     echo "Before unset:$var,";
     unset($bar);
     $var=23;
     echo "after unset:$var\n";
 }
uns();
uns();
uns();
echo "<br>use of isset function<br>";

$v="34";
if(isset($v))
{
    echo "this is set";
}
unset($v);
var_dump(isset($v));
echo ($v);
echo "<br>use of unserialize function<br>";
$serialized_data = serialize(array('Math', 'Language', 'Science'));
echo  $serialized_data . '<br>';
// Unserialize the data
$var1 = unserialize($serialized_data);
// Show the unserialized data;
var_dump ($var1);
?>

